<?
/* Тестовое задание 2
 * Скрипт для добавления товара и торговых предложений
 * Предусмотрен выбор раздела каталога, ввод названия товара, производителя и описания
 * Для предложений ввод артикула
 * Предполагается что инфоблоки правильно настроены
 * Данный скрипт работоспособен для примера магазина из поставки Bitrix
 */
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

//настройки:
$IDIB_Product=2; // ID инфоблока товаров
$IDIB_Offer=3; // ID инфоблока предложений
$CODE_SKU_LINK='CML2_LINK'; // Код свойства привязки предложений к товарам
$CODE_Vendor='MANUFACTURER'; // код свойства товара "Производитель"
$CODE_Art='ARTNUMBER'; // код свойства предложения "Артикул"

// Подключаем модули
CModule::IncludeModule('iblock');

$ID_prod=$_GET['product']; // получаем ID товара, если задан
if(empty($_POST)) { //если нет данных POST из форм
    if(empty($ID_prod)) { //если ничего не получено, выводим форму добавления товара
        ?>
        <h1>Добавление товара</h1>
        <form method="post">
            Раздел
            <select name='section_id'>
                <?
                $arFilter = array('IBLOCK_ID' => $IDIB_Product, 'ACTIVE' => 'Y');
                $arSelect = array('ID', 'NAME', 'DEPTH_LEVEL');
                $oSection = CIBlockSection::GetTreeList($arFilter, $arSelect);
                while ($arSection = $oSection->Fetch()) {
                    $mark_level = "";
                    for ($i = 2; $i <= $arSection['DEPTH_LEVEL']; $i++)
                        $mark_level .= '-';
                    ?>
                    <option value="<?= $arSection['ID']; ?>"><?= $mark_level . $arSection['NAME']; ?></option>
                <?
                } ?>
            </select><br>
            Название <input type="text" name="name" value=""><br>
            Производитель <input type="text" name="vendor" value=""><br>
            Описание<br>
            <textarea name="description"></textarea><br>
            <input type="submit" value="Сохранить">
        </form>
        <?
    } else { //если есть ID товара, выводим форму добавления торгового предложения
        $arFilter = array('IBLOCK_ID' => $IDIB_Product, 'ID' => $ID_prod);
        $oProd = CIBlockElement::GetList(array(), $arFilter, false, false, array('NAME','CODE'));
        $arFields = $oProd->GetNextElement()->GetFields();
        ?>
        <h1>Добавление торгового предложения к товару<br>
            &quot;<?= $arFields['NAME'] ?>&quot;</h1>
        <form method="post" action="?product=<?= $ID_prod ?>">
            <input type="hidden"  name="name" value="<?= $arFields['NAME'] ?>">
            <input type="hidden"  name="code" value="<?= $arFields['CODE'] ?>">
        Артикул <input type="text" name="art" value=""><br>
        <input type="submit" value="Сохранить">
        </form>
        <br><a href="<?= $_SERVER["SCRIPT_NAME"] ?>">Новый товар</a>
        <?
    }
} else { // есть данные из формы
    if(empty($ID_prod)) { // если нет ID товара, сохраняем товар
        $oProd = new CIBlockElement();
        $arFields = array(
            'NAME' => $_POST['name'],
            'CODE' => CUtil::translit($_POST['name'],'ru',array('replace_space' => '-')),
            'IBLOCK_ID' => $IDIB_Product,
            'IBLOCK_SECTION_ID' => $_POST['section_id'],
            'ACTIVE' => 'Y',
            'DETAIL_TEXT' => $_POST['description'],
            'PROPERTY_VALUES' => array(
              'MANUFACTURER' => $_POST['vendor']
            ),
        );
        $ID_prod = $oProd->Add($arFields);
        header('Location: '.$_SERVER["SCRIPT_NAME"].'?product='.$ID_prod); // редирект на форму добавления торговых предложений
    } else { // если передан ID товара, сохраняем торговое предложение
        $oOffer = new CIBlockElement();
        $arFields = array(
            'NAME' => $_POST['name'],
            'CODE' => $_POST['code'].'_'.CUtil::translit($_POST['art'],'ru',array('replace_space' => '-')),
            'IBLOCK_ID' => $IDIB_Offer,
            'ACTIVE' => 'Y',
            'PROPERTY_VALUES' => array(
                $CODE_SKU_LINK => $ID_prod,
                'ARTNUMBER' => $_POST['art'],
            ),
        );
        $oOffer->Add($arFields);
        header('Location: '.$_SERVER["SCRIPT_NAME"].'?product='.$ID_prod); // возможность добавить еще предложение
    }
}
?>

