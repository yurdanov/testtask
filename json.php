<?
/*
 * Тестовое задание 3
 * Скрипт для обработки JSON-массива
 */
if(($str=file_get_contents('chart.json')) === false) {  //получаем исходные данные
    echo 'File chart.json not found';
    die();
}
if(($arr=json_decode($str)) == null) { //декодируем исходные данные в массив
    echo 'Incorrect format of the source file';
    die();
}
foreach($arr as &$col) { //цикл по колонкам
    if(count($col)<5) //если в колонке меньше 4 значений, то точно ничего менять не надо
        continue;
    $flag=true; //"флаг" необходимости замен для колонки. по умолчанию - истина
    for($i=1;$i<=4;$i++) { //просмотр первых 4 значений
        if($col[$i] != 100) { //если хоть одно из первых 4-х не 100
            $flag=false; //то ничего менять не надо
            break;
        }
    }
    if($flag) { //если к этой точке flag все еще "истина", надо менять начиная с 1
        for($i=1;$i<count($col);$i++) { //цикл по значениям в колонке
            if($col[$i] == 100)
                $col[$i]=null; // если зачение 100, меняем
            else
                break; // если не 100, прекращаем обработку колонки
        }
    }
}
$str=json_encode($arr); //кодируем массив обратно в json
file_put_contents('chart_result.json',$str); //сохраняем файл результата
echo 'processing completed successfully';
?>