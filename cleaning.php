<?
/*
 * Тестовое задание 1
 * Очистка каталога от лишних ненужных картинок
 *
 * Алгоритм работы:
 * Полный просмотр каталога, в котором хранятся картинки
 * и поиск ссылки на каждый файл в БД.
 * Если на файл по каким-то причинам потеряна ссылка, то удаление.
 * Файл не считается "потерянным" если на его ID есть ссылки в "PREVIEW_PICTURE" или "DETAIL_PICTURE" элементов инфоблоков.
 * Также не считается потерянным если на его ID ссылается свойство типа файл "привязанное" к существующему элементу.
 * Перед просмотром каталога, создаем временную таблицу в БД с ID всех файлов, на которые ссылаются элементы инфоблоков.
 *
 * Отказ от использования каких-либо массивов осознаный, по следующим причинам:
 * - операция очистки скорее всего необходима редко, быстродействие не критично
 * - сильно загружать процессор и память сервера операцией очистки не желательно
 */

//параметры подключения к БД
$db_host='localhost';
$db_name = 'sitemanager';
$db_username = 'bitrix0';
$db_password = '_?uyGm[+D8I87uZfdf-f';
//каталог хранения картинок для инфоблоков
$root_dir_name=$_SERVER["DOCUMENT_ROOT"]."/upload/iblock";

$mysqli = new mysqli($db_host, $db_username, $db_password, $db_name);
if ($mysqli->connect_errno) {
    echo "Ошибка поключения к БД";
    die();
}
/* создаем временную таблицу с ID всех файлов упоминающихся в элементах инфоблоков */
$sql=<<<SQL
CREATE TEMPORARY TABLE `tmp_ib_img` (
SELECT FLOOR(v.`VALUE_NUM`) as `ID`
FROM `b_iblock_element` e, `b_iblock_element_property` v, `b_iblock_property` p
WHERE v.`IBLOCK_ELEMENT_ID` = e.`ID`
AND v.`IBLOCK_PROPERTY_ID` = p.`ID` AND p.`PROPERTY_TYPE` = 'F' )
UNION
SELECT `PREVIEW_PICTURE` as `ID` FROM `b_iblock_element`
UNION
SELECT `DETAIL_PICTURE` as `ID` FROM `b_iblock_element`
SQL;
$mysqli->query($sql);

// создаем "шаблон" SQL-запроса
$sql=<<<SQL
SELECT `ID`
FROM `b_file`
WHERE `ID` IN (SELECT `ID` FROM `tmp_ib_img`)
AND `FILE_NAME` = '
SQL;

/* просматриваем все каталоги */
$rDir = opendir($root_dir_name);
while (false !== ($cdName = readdir($rDir))) { // цикл по вложенным каталогам
    if ($cdName == '.' || $cdName == '..')
        continue;
    $cDir = opendir($root_dir_name . '/' . $cdName);
    while (false !== ($cfName = readdir($cDir))) { // цикл по файлам в каталоге
        if ($cfName == '.' || $cfName == '..')
            continue;
        $stmt=$mysqli->query($sql.$cfName."'");
        if($stmt->num_rows == 0) {// если во временной таблице упоминания о данном файле нет или его вообще нет в b_file
            echo "Deleting " . $cfName . "<br>";
            unlink($root_dir_name . '/' . $cdName . '/' . $cfName); // удаляем файл
        }
    }
    closedir($cDir);
}
closedir($rDir);
?>
